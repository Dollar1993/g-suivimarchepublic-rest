package sn.master;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import sn.master.entities.User;
import sn.master.services.IServiceUtilisateur;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GSuivimarchepublicRestApplicationTests {

	@Autowired
	private IServiceUtilisateur serviceUtilisateur;
	@Test
	public void contextLoads() {
		User user1 = new User();
		user1.setMail("admin@gmail.com");
		user1.setPrenom("admin");
		user1.setNom("admin");
		user1.setActive(1);
		user1.setPassword("123");		
		serviceUtilisateur.createAdmin(user1);

        assertEquals("Test sur insertion de admin 1","admin@gmail.com",user1.getMail());
	}
}
