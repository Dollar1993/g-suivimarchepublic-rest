package sn.master.repositories;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import sn.master.entities.Structure;

@RepositoryRestResource
public interface ServiceRepository extends JpaRepository<Structure, String> {
  
}
