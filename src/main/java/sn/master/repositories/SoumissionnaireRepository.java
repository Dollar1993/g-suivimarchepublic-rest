package sn.master.repositories;




import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import sn.master.entities.Soumissionaire;

@RepositoryRestResource
public interface SoumissionnaireRepository extends JpaRepository<Soumissionaire, String>
{
}
