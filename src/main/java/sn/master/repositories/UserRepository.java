package sn.master.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import sn.master.entities.User;


@RepositoryRestResource
public interface UserRepository extends JpaRepository<User,String>
{
	User findByMailAndPassword(String mail, String password);

}
