package sn.master.repositories;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import sn.master.entities.Journal;

@RepositoryRestResource
public interface JournalRepository extends JpaRepository<Journal, String>
{
	
}
