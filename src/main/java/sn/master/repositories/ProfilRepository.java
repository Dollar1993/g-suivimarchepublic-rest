package sn.master.repositories;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import sn.master.entities.Profil;



@RepositoryRestResource
public interface ProfilRepository extends JpaRepository<Profil, String>
{
	Profil findByUsersMailAndUsersPassword(String mail, String password);
	
}
