package sn.master.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import sn.master.entities.Besoin;

@RepositoryRestResource
public interface BesoinRepository extends JpaRepository<Besoin, Integer> {
    
}
