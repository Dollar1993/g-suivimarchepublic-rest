package sn.master.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import sn.master.entities.User;
import sn.master.services.impl.ServiceUtilisateurImpl;

@RestController
public class UtilisateurController {
	@Autowired
	private ServiceUtilisateurImpl serviceUtilisateurImpl;
	
	@GetMapping("/users")
	public List<User> findAll(){
		return serviceUtilisateurImpl.findAllUser();
	}
	
	@PostMapping("users/admin")
	public void createUser(@RequestBody User user) {
			serviceUtilisateurImpl.createAdmin(user);
	}
	@DeleteMapping("/users/{mail}")
	public void deleteUser(@PathVariable String mail) {
		serviceUtilisateurImpl.deleteUser(mail);
	}
	

}
