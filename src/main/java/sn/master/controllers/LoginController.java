package sn.master.controllers;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import sn.master.entities.Profil;
import sn.master.services.impl.ServiceUtilisateurImpl;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class LoginController {
	@Autowired
	private ServiceUtilisateurImpl serviceUtilisateurImpl;
	
	@GetMapping("/users/{mail}/{password}")
	public Profil findLogin(@PathVariable String mail, @PathVariable String password){
		return serviceUtilisateurImpl.login(mail, password);
	}
	

}
