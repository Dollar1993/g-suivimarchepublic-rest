package sn.master.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sn.master.entities.Profil;
import sn.master.entities.User;
import sn.master.repositories.ProfilRepository;
import sn.master.repositories.UserRepository;
import sn.master.services.IServiceUtilisateur;

@Service
public class ServiceUtilisateurImpl implements IServiceUtilisateur  {
    @Autowired
    private UserRepository userRepository;
    @Autowired ProfilRepository profilRepository;

    @Override
    public void createAdmin(User user) {
        
        // Specifier le role du user
        Profil adminRole = new Profil();
        adminRole.setName("ADMIN");
        List<Profil> roles = user.getUserprofils();
        if (roles==null) {
        	roles = new ArrayList<Profil>();
        }
        roles.add(adminRole);
        user.setUserprofils(roles);
        
        userRepository.save(user);
    }

	@Override
	public void createSoum(User user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createUsersCommission(User user) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createUsersService(User user) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public List<User> findAllUser() {
		return userRepository.findAll();
	}

	@Override
	public void deleteUser(String mail) {
		userRepository.deleteById(mail);
		
	}

	@Override
	public boolean isUserPresent(String mail) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<User> usersService(String codeService) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getUser(String mail) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Profil login(String mail, String password) {
		return profilRepository.findByUsersMailAndUsersPassword(mail, password);
	}


}
