package sn.master.services;



import java.util.List;

import sn.master.entities.Profil;
import sn.master.entities.Structure;
import sn.master.entities.User;

public interface IServiceUtilisateur {
	
	
    public void createAdmin(User user);
    
    public void createSoum(User user);
    
    public void createUsersCommission(User user);
    
    public void createUsersService(User user);
	
	public List<User> findAllUser();
	
	public void deleteUser(String mail);
		
	public boolean isUserPresent(String mail);
	
	public List<User> usersService(String codeService);
	
	public User getUser(String mail);
	
	public Profil login(String mail,String password);
	
}
